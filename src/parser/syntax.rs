/*!
A recursive descent parser to build an isotope AST
*/
use internship::IStr;
use hashbrown::hash_map::{HashMap, Entry};
use nom::{
    IResult,
    combinator::{map, map_res, opt},
    branch::alt,
    character::complete::{digit1, multispace0},
    sequence::{preceded, terminated, delimited, tuple},
    bytes::complete::{tag, is_not},
    multi::many0
};

use crate::TupleIx;
use super::ast::Expr;

const SPECIAL_CHARS: &str = "#(){}: \t\r\n";

/// Attempt to parse a string into an expr
pub fn parse_expr(input: &str) -> IResult<&str, Expr> {
    delimited(
        multispace0,
        alt((
            parse_sexpr,
            parse_tuple,
            preceded(tag("#"), parse_special),
            map(parse_symbol, |symbol| Expr::Symbol(symbol))
        )),
        multispace0
    )(input)
}

pub fn parse_sexpr(input: &str) -> IResult<&str, Expr> {
    map(
        delimited(tag("("), delimited(multispace0, parse_list, multispace0), tag(")")),
        |exprs| Expr::Sexpr(exprs)
    )(input)
}
pub fn parse_tuple(input: &str) -> IResult<&str, Expr> {
    map(
        delimited(tag("{"), delimited(multispace0, parse_list, multispace0), tag("}")),
        |exprs| Expr::Tuple(exprs)
    )(input)
}
pub fn parse_symbol(input: &str) -> IResult<&str, IStr> {
    map(
        is_not(SPECIAL_CHARS),
        |string| IStr::new(string)
    )(input)
}

pub fn parse_special(input: &str) -> IResult<&str, Expr> {
    alt((
        preceded(tag("struct"), parse_struct),
        preceded(tag("product"), parse_product),
        preceded(tag("sum"), parse_sum),
        preceded(tag("define"), delimited(
            terminated(tag("("), multispace0), parse_definition, preceded(multispace0, tag(")")))),
        preceded(tag("t_define"), delimited(
            terminated(tag("("), multispace0),
            parse_transitive_definition,
            preceded(multispace0, tag(")")))),
        preceded(tag("lambda"), delimited(
            terminated(tag("("), multispace0), parse_lambda, preceded(multispace0, tag(")")))),
        preceded(tag("get"), delimited(
            terminated(tag("("), multispace0), parse_member, preceded(multispace0, tag(")")))),
        preceded(tag("proj"), delimited(
            terminated(tag("("), multispace0), parse_projection, preceded(multispace0, tag(")")))),
        preceded(tag("inj"), delimited(
            terminated(tag("("), multispace0), parse_injection, preceded(multispace0, tag(")")))),
        preceded(tag("data"), delimited(
            terminated(tag("("), multispace0), parse_data, preceded(multispace0, tag(")")))),
        preceded(tag("typeof"), delimited(
            terminated(tag("("), multispace0), parse_typeof, preceded(multispace0, tag(")")))),
        preceded(tag("asserty"), delimited(
            terminated(tag("("), multispace0), parse_asserty, preceded(multispace0, tag(")"))))
    ))(input)
}
pub fn parse_list(input: &str) -> IResult<&str, Vec<Expr>> {
    many0(parse_expr)(input)
}
pub fn parse_members(input: &str) -> IResult<&str, HashMap<IStr, Expr>>{
    map_res(
        many0(parse_set_member),
        |members| {
            let mut res = HashMap::with_capacity(members.len());
            for member in members {
                match res.entry(member.0) {
                    //TODO: better errors
                    Entry::Occupied(_) => { return Err(()) },
                    Entry::Vacant(v) => { v.insert(member.1); },
                }
            }
            Ok(res)
        }
    )(input)
}
pub fn parse_set_member(input: &str) -> IResult<&str, (IStr, Expr)> {
    map(
        tuple((delimited(multispace0, parse_symbol, multispace0), tag(":"), parse_expr)),
        |(sym, _, expr)| (sym, expr)
    )(input)
}

pub fn parse_record(input: &str) -> IResult<&str, Expr> {
    map(
        delimited(tag("{"), parse_members, tag("}")),
        |members| Expr::Record(members)
    )(input)
}
pub fn parse_definition(input: &str) -> IResult<&str, Expr> {
    map(
        tuple((preceded(multispace0, parse_symbol), parse_expr)),
        |(name, expr)| Expr::Definition(name, Box::new(expr), false)
    )(input)
}
pub fn parse_transitive_definition(input: &str) -> IResult<&str, Expr> {
    map(
        tuple((preceded(multispace0, parse_symbol), parse_expr)),
        |(name, expr)| Expr::Definition(name, Box::new(expr), true)
    )(input)
}
pub fn parse_lambda(input: &str) -> IResult<&str, Expr> {
    map(
        tuple((
            preceded(multispace0, delimited(tag("{"), parse_members, tag("}"))),
            parse_expr,
            alt((
                map(
                    terminated(delimited(multispace0, tag("->"), multispace0), tag("?")),
                    |_| None
                ),
                opt(preceded(tuple((multispace0, tag("->"))), parse_expr))
            ))
        )),
        |(params, expr, ty)| Expr::Lambda(params, Box::new(expr), ty.map(|ty| Box::new(ty)))
    )(input)
}
pub fn parse_member(input: &str) -> IResult<&str, Expr> {
    map(
        tuple((
            preceded(multispace0, parse_symbol), parse_expr
        )),
        |(name, expr)| Expr::Member(name, Box::new(expr))
    )(input)
}
pub fn parse_tuple_ix(input: &str) -> IResult<&str, TupleIx> {
    map_res(
        digit1,
        |digits: &str| digits.parse::<TupleIx>()
    )(input)
}
pub fn parse_projection(input: &str) -> IResult<&str, Expr> {
    map(
        tuple((
            preceded(multispace0, parse_tuple_ix), parse_expr
        )),
        |(ix, expr)| Expr::Projection(ix, Box::new(expr))
    )(input)
}
pub fn parse_injection(input: &str) -> IResult<&str, Expr> {
    map(
        tuple((
            preceded(multispace0, parse_tuple_ix), parse_expr
        )),
        |(ix, expr)| Expr::Injection(ix, Box::new(expr))
    )(input)
}
pub fn parse_data(input: &str) -> IResult<&str, Expr> {
    map(
        tuple((
            preceded(multispace0, parse_symbol),
            delimited(multispace0, delimited(tag("{"), parse_members, tag("}")), multispace0)
        )),
        |(name, constructors)| Expr::Data(name, constructors)
    )(input)
}
pub fn parse_typeof(input: &str) -> IResult<&str, Expr> {
    map(
        parse_expr,
        |expr| Expr::TypeOf(Box::new(expr))
    )(input)
}
pub fn parse_asserty(input: &str) -> IResult<&str, Expr> {
    map(
        tuple((parse_expr, parse_expr)),
        |(expr, ty)| Expr::AssertTy(Box::new(expr), Box::new(ty))
    )(input)
}
pub fn parse_product(input: &str) -> IResult<&str, Expr> {
    map(
        delimited(tag("{"), parse_list, tag("}")),
        |members| Expr::Product(members)
    )(input)
}
pub fn parse_struct(input: &str) -> IResult<&str, Expr> {
    map(
        delimited(tag("{"), parse_members, tag("}")),
        |members| Expr::Struct(members)
    )(input)
}
pub fn parse_sum(input: &str) -> IResult<&str, Expr> {
    map(
        delimited(tag("{"), parse_list, tag("}")),
        |members| Expr::Sum(members)
    )(input)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn nil_parses_properly() {
        assert_eq!(
            ("", Expr::Sexpr(vec![])),
            parse_expr("()").unwrap()
        );
        assert_eq!(
            ("", Expr::Tuple(vec![])),
            parse_expr("{}").unwrap()
        );
    }
    #[test]
    fn arithmetic_parses_properly() {
        let parsed_sexpr = parse_expr("(+ x (* y (+ (+ z x) (/ w x))))").unwrap();
        assert_eq!(
            parsed_sexpr,
            ("", Expr::Sexpr(vec![
                Expr::sym("+"),
                Expr::sym("x"),
                Expr::Sexpr(vec![
                    Expr::sym("*"),
                    Expr::sym("y"),
                    Expr::Sexpr(vec![
                        Expr::sym("+"),
                        Expr::Sexpr(vec![
                            Expr::sym("+"),
                            Expr::sym("z"),
                            Expr::sym("x")
                            ]),
                        Expr::Sexpr(vec![
                            Expr::sym("/"),
                            Expr::sym("w"),
                            Expr::sym("x")
                            ])
                    ])
                ])]))
        );
        assert_eq!(
            parse_expr(&format!("{}", parsed_sexpr.1)).unwrap(),
            parsed_sexpr
        );
    }
    #[test]
    fn products_parse_properly() {
        let parsed_product = parse_expr("#product{x y z}").unwrap();
        assert_eq!(
            parsed_product,
            ("", Expr::Product(vec![Expr::sym("x"), Expr::sym("y"), Expr::sym("z")]))
        );
    }
    #[test]
    fn projections_parse_properly() {
        let parsed_product = parse_expr("#proj(1 {- +})").unwrap();
        assert_eq!(
            parsed_product,
            ("", Expr::Projection(1, Expr::Tuple(vec![Expr::sym("-"), Expr::sym("+")]).into()))
        );
    }
}
