/*!
An experimental parser for isotope
*/
use internship::IStr;
use derive_more::From;
use hashbrown::hash_map::HashMap;

use crate::TupleIx;
use crate::builder::{
    tuple::BuildTupleError,
    expr::BuildExprError,
    product::BuildProductError,
    projection::BuildProjError,
    ContextBuilder
};
use crate::context::{
    Context, ValId, display::TryDisplay
};

pub mod ast;
use ast::Expr;
pub mod syntax;
use syntax::parse_expr;

/// Standard isotope parser metadata
#[derive(Debug, Clone)]
pub struct StandardMetadata {
    pub names: Vec<IStr>
}

impl TryDisplay for StandardMetadata {
    type AsDisplay = str;
    #[inline] fn to_disp(&self) -> Option<&str> { self.names.last().map(|s| s.as_str()) }
}

impl From<IStr> for StandardMetadata {
    #[inline] fn from(name: IStr) -> Self { StandardMetadata { names : vec![name] } }
}

pub trait ParserMetadata: From<StandardMetadata> {
    fn append(&mut self, other: Self);
}

impl ParserMetadata for StandardMetadata {
    fn append(&mut self, other: StandardMetadata) {
        self.names.extend(other.names.into_iter())
    }
}

/// An isotope parsing error
#[derive(Debug, Clone, Eq, PartialEq)]
#[derive(From)]
pub enum ParseError {
    SemanticError(SemanticError),
    SyntaxError
}

/// An isotope semantic error
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum SemanticError {
    InvalidProj{ output: TupleIx, nesting: u32 },
    UnknownVariable(IStr)
}

impl From<BuildTupleError> for SemanticError {
    fn from(err: BuildTupleError) -> SemanticError { match err {} }
}
impl From<BuildExprError> for SemanticError {
    fn from(err: BuildExprError) -> SemanticError { match err {} }
}
impl From<BuildProductError> for SemanticError {
    fn from(err: BuildProductError) -> SemanticError { match err {} }
}
impl From<BuildProjError> for SemanticError {
    fn from(err: BuildProjError) -> SemanticError {
        use BuildProjError::*;
        match err {
            InvalidProj{ output, nesting } => Self::InvalidProj{ output, nesting }
        }
    }
}

/// An isotope parser containing a context builder
#[derive(Debug)]
pub struct Parser<'a, M = StandardMetadata> {
    /// The `ContextBuilder` to hold parsed values
    builder: ContextBuilder<'a, M>,
    /// The current scope depth
    scope_depth: u32,
    /// A hashmap of names to scoped definitions
    definitions: HashMap<IStr, Vec<(u32, ValId)>>
}

impl<'a, M> Parser<'a, M> {
    /// Build a new isotope parser with the given context builder
    pub fn new(builder: ContextBuilder<'a, M>) -> Self {
        Parser { builder, scope_depth : 0, definitions: HashMap::new() } }
    /// A utility method to build a new isotope parser borrowing a given context.
    /// Makes an empty `ContextBuilder` from the borrowed context.
    pub fn from_ctx(ctx: &'a mut Context<M>) -> Self { Self::new(ContextBuilder::new(ctx)) }
    /// Get the current definition of a name
    pub fn get_named(&mut self, name: &IStr) -> Option<ValId> {
        let name_defs = self.definitions.get_mut(name)?;
        Self::clean_defs(name_defs, self.scope_depth);
        name_defs.last().map(|(_, v)| *v)
    }
    /// Get the current definition of a name from something converting to an IStr
    pub fn get_def<I: Into<IStr>>(&mut self, name: I) -> Option<ValId> {
        self.get_named(&name.into())
    }
    /// Clean out a definition vector given the current scope depth
    pub fn clean_defs(defs: &mut Vec<(u32, ValId)>, scope_depth: u32) {
        while let Some((level, _)) = defs.last() {
            if *level <= scope_depth { return }
            defs.pop();
        }
    }
    /// Peek at the current definition of a name
    pub fn peek_named(&mut self, name: IStr) -> Option<ValId> {
        let name_defs = self.definitions.get(&name)?;
        for (level, def) in name_defs.iter().rev() {
            if *level <= self.scope_depth { return Some(*def) }
        }
        return None
    }
    /// Get the builder underlying this parser
    #[inline] pub fn builder(&self) -> &ContextBuilder<'a, M> { &self.builder }
    /// Get a mutable reference to the builder underlying this parser
    #[inline] pub fn builder_mut(&mut self) -> &mut ContextBuilder<'a, M> { &mut self.builder }
    /// Get the context underlying this parser
    #[inline] pub fn ctx(&self) -> &Context<M> { self.builder.ctx() }
}

impl<'a, M> Parser<'a, M> where M: ParserMetadata {
    /// Parse a string into a value
    pub fn parse<'i>(&mut self, input: &'i str) -> Result<(&'i str, ValId), ParseError> {
        match parse_expr(input) {
            Ok((rest, expr)) => {
                self.parse_ast(expr)
                    .map(|val| (rest, val))
                    .map_err(|err| err.into())
            },
            Err(_err) => {
                Err(ParseError::SyntaxError)
            }
        }
    }
    /// Parse an iterator of expressions into a vector of values
    pub fn parse_ast_iter(&mut self, exprs: Vec<Expr>) -> Result<Vec<ValId>, SemanticError> {
        exprs.into_iter()
            .map(|expr| self.parse_ast(expr))
            .collect()
    }
    /// Parse an expression into a value
    pub fn parse_ast(&mut self, expr: Expr) -> Result<ValId, SemanticError> {
        match expr {
            Expr::Sexpr(s) => {
                let args = self.parse_ast_iter(s)?;
                self.builder.build_expr(args.into_iter()).map_err(|err| err.into())
            },
            Expr::Tuple(t) => {
                let mems = self.parse_ast_iter(t)?;
                self.builder.build_tuple(mems.into_iter()).map_err(|err| err.into())
            },
            Expr::Product(p) => {
                let mems = self.parse_ast_iter(p)?;
                self.builder.build_product(mems.into_iter()).map_err(|err| err.into())
            },
            Expr::Projection(o, e) => {
                let arg = self.parse_ast(*e)?;
                self.builder.build_projection(arg, o).map_err(|err| err.into())
            },
            Expr::Definition(name, e, transitive) => {
                let def = self.parse_ast(*e)?;
                let name_defs = self.definitions.entry(name.clone())
                    .or_insert(Vec::with_capacity(1));
                Self::clean_defs(name_defs, self.scope_depth);
                name_defs.push((self.scope_depth, def));
                let name_meta = StandardMetadata::from(name).into();
                match self.builder.get_meta_mut(def) {
                    Some(meta) => meta.append(name_meta),
                    None => { let _ = self.builder.try_set_meta(def, Some(name_meta)); }
                };
                if transitive { Ok(def) } else { Ok(self.builder.nil()) }
            },
            Expr::Symbol(name) => {
                self.get_named(&name).ok_or(SemanticError::UnknownVariable(name))
            },
            _ => unimplemented!()
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::context::{Context, display::{arity_display, simple_rec_display}};
    use super::*;
    #[test]
    fn nil_tuples_are_parsed_properly() {
        let mut ctx = Context::<StandardMetadata>::with_capacity(6, 6);
        let mut parser = Parser::from_ctx(&mut ctx);
        assert_eq!(
            parser.parse("{}").expect("Nil is a valid expression"),
            ("", parser.builder().nil())
        )
    }
    #[test]
    fn tuples_of_nil_tuples_are_parsed_properly() {
        let mut ctx = Context::<StandardMetadata>::with_capacity(10, 10);
        let mut parser = Parser::from_ctx(&mut ctx);
        assert_eq!(
            parser.parse("{{}}").expect("Nested nil is a valid expression"),
            ("", parser.builder().nil())
        );
        assert_eq!(
            parser.parse("{{ {{}  }} }").expect("Deep nested nil is a valid expression"),
            ("", parser.builder().nil())
        );
        let (_, parsed) = parser.parse("{  {}  { }}").expect("Tuple of nil is a valid expression");
        assert_eq!(
            &format!("{}", ctx.bind(parsed).to_disp(&arity_display)),
            "{* *}"
        )
    }
    #[test]
    fn trees_of_nil_tuples_are_parsed_properly() {
        let mut ctx = Context::<StandardMetadata>::with_capacity(10, 10);
        let mut parser = Parser::from_ctx(&mut ctx);
        let (_, parsed) = parser.parse("{{{{{}}}} {{ {{}} {{{{}}}} {}}} {{ }}  { }}")
            .expect("Nil tree is a valid expression");
        assert_eq!(
            &format!("{}", ctx.bind(parsed).to_disp(&arity_display)),
            "{* * * *}"
        );
        assert_eq!(
            &format!("{}", ctx.bind(parsed).to_disp(&simple_rec_display)),
            "{{} {{} {} {}} {} {}}"
        )
    }
    #[test]
    fn nil_exprs_are_parsed_properly() {
        let mut ctx = Context::<StandardMetadata>::with_capacity(6, 6);
        let mut parser = Parser::from_ctx(&mut ctx);
        assert_eq!(
            parser.parse("()").expect("Nil is a valid expression"),
            ("", parser.builder().nil())
        )
    }
    #[test]
    fn unit_type_is_parsed_properly() {
        let mut ctx = Context::<StandardMetadata>::with_capacity(6, 6);
        let mut parser = Parser::from_ctx(&mut ctx);
        assert_eq!(
            parser.parse("#product{}").expect("Unit is a valid expression"),
            ("", parser.builder().unit())
        )
    }
    #[test]
    fn tuple_of_unit_types_is_parsed_properly() {
        let mut ctx = Context::<StandardMetadata>::with_capacity(6, 6);
        let mut parser = Parser::from_ctx(&mut ctx);
        assert_eq!(
            parser.parse("{#product{}}").expect("Unit tuple is a valid expression"),
            ("", parser.builder().unit())
        );
        assert_ne!(
            parser.parse("{#product{} #product{}}").expect("Unit tuple is a valid expression"),
            ("", parser.builder().unit())
        )
    }
    #[test]
    fn projections_of_nil_parse_and_normalize() {
        let mut ctx = Context::<StandardMetadata>::with_capacity(10, 10);
        let mut parser = Parser::from_ctx(&mut ctx);
        assert_eq!(
            parser.parse("#proj(2 { () (()) ((())) })").expect("Projection of nil is valid"),
            ("", parser.builder().nil())
        );
        assert_eq!(
            parser.parse("#proj(3 { () () })"),
            Err(ParseError::SemanticError(SemanticError::InvalidProj{
                output : 3, nesting : 0
            }))
        )
    }
    #[test]
    fn definitions_parse_and_normalize() {
        let mut ctx = Context::<StandardMetadata>::with_capacity(10, 10);
        let mut parser = Parser::from_ctx(&mut ctx);
        assert_eq!(
            parser.parse("#define(x { () })").expect("Projection of nil is valid"),
            ("", parser.builder().nil())
        );
        assert_eq!(parser.get_def("x"), Some(parser.builder().nil()));
    }
    #[test]
    fn simple_lookup_works() {
        let mut ctx = Context::<StandardMetadata>::with_capacity(10, 10);
        let mut parser = Parser::from_ctx(&mut ctx);
        let parsed = parser.parse("#t_define(x { #t_define(y ()) #t_define(z {{} {}}) {{}} })")
            .expect("Nested definitions are valid").1;
        assert_eq!(parsed, parser.get_def("x").unwrap());
        assert_eq!(parser.get_def("y"), Some(parser.builder().nil()));
        assert_ne!(parser.get_def("z"), None);
        assert_eq!(parser.get_def("w"), None);
        let proj = parser.parse("#proj(0 x)").expect("Projections of symbols are valid").1;
        assert_eq!(proj, parser.get_def("y").unwrap());
        let proj = parser.parse("#proj(1 x)").expect("Projections of symbols are valid").1;
        assert_eq!(proj, parser.get_def("z").unwrap());
        assert_eq!(
            parser.parse("#proj(3 w)"), Err(SemanticError::UnknownVariable(IStr::new("w")).into())
        );
    }
}
