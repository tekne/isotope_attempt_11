/*!
The isotope AST
*/
use std::fmt::{self, Formatter, Display};
use internship::IStr;
use hashbrown::hash_map::HashMap;

use crate::TupleIx;

/// An isotope AST.
//TODO: metadata?
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Expr {
    /// An S-expression. The empty S-expression is nil.
    Sexpr(Vec<Expr>),
    /// A tuple. The empty tuple is also nil, but it is normalized to a sexpr.
    Tuple(Vec<Expr>),
    /// A record. The empty struct is also nil, but it is normalized to a sexpr.
    Record(HashMap<IStr, Expr>),
    /// A definition. Associates a symbol and expression. Boolean indicates if it's transitive.
    Definition(IStr, Box<Expr>, bool),
    /// A lambda function. Associates a set of typed parameters with an expression.
    /// Optionally with an asserted result type
    Lambda(HashMap<IStr, Expr>, Box<Expr>, Option<Box<Expr>>),
    /// Get an element of a struct
    Member(IStr, Box<Expr>),
    /// Project out of a tuple
    Projection(TupleIx, Box<Expr>),
    /// Inject into a sum type
    Injection(TupleIx, Box<Expr>),
    /// Define an inductive type, given a map of constructor names to types.
    /// Types must be strictly positive in the defined data type.
    Data(IStr, HashMap<IStr, Expr>),
    /// Get the type of a value
    TypeOf(Box<Expr>),
    /// Assert an expression is of a given type
    AssertTy(Box<Expr>, Box<Expr>),
    /// Take the product of a set of types
    Product(Vec<Expr>),
    /// Make a struct, mapping names to types
    Struct(HashMap<IStr, Expr>),
    /// Take the sum of a set of types
    Sum(Vec<Expr>),
    /// A polymorphic universe
    Universe,
    /// Define the function type between
    /// A symbol, represented as a string
    Symbol(IStr)
    //TODO: numbers, strings, etc.
}

impl Expr {
    pub fn nil() -> Expr { Expr::Sexpr(vec![]) }
    pub fn sym<I: Into<IStr>>(s: I) -> Expr { Expr::Symbol(s.into()) }
}


impl Display for Expr {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        match self {
            Expr::Sexpr(args) => {
                write!(fmt, "(")?;
                display_expr_list(args.iter(), fmt)?;
                write!(fmt, ")")
            },
            Expr::Tuple(members) => {
                write!(fmt, "{{")?;
                display_expr_list(members.iter(), fmt)?;
                write!(fmt, "}}")
            },
            Expr::Record(members) => {
                write!(fmt, "#record{{")?;
                display_members(members.iter(), fmt)?;
                write!(fmt, "}}")
            },
            Expr::Definition(name, definition, transitive) => {
                if *transitive {
                    write!(fmt, "#define({} {})", name, definition)
                } else {
                    write!(fmt, "#t_define({} {})", name, definition)
                }
            },
            Expr::Lambda(params, expression, ty) => {
                write!(fmt, "#lambda({{")?;
                display_members(params.iter(), fmt)?;
                write!(fmt, "}} {} -> ", expression)?;
                if let Some(ty) = ty {
                    write!(fmt, "{})", ty)
                } else {
                    write!(fmt, "?)")
                }
            },
            Expr::Member(name, from) => {
                write!(fmt, "#get({} {})", name, from)
            },
            Expr::Projection(ix, from) => {
                write!(fmt, "#proj({} {})", ix, from)
            },
            Expr::Injection(ix, from) => {
                write!(fmt, "#inj({} {})", ix, from)
            },
            Expr::Data(name, constructors) => {
                write!(fmt, "#data({} ", name)?;
                write!(fmt, "{{")?;
                display_members(constructors.iter(), fmt)?;
                write!(fmt, "}})")
            },
            Expr::TypeOf(expr) => {
                write!(fmt, "#type({})", expr)
            },
            Expr::AssertTy(expr, ty) => {
                write!(fmt , "#is({} {})", expr, ty)
            },
            Expr::Product(types) => {
                write!(fmt, "#product{{")?;
                display_expr_list(types.iter(), fmt)?;
                write!(fmt, "}}")
            },
            Expr::Struct(types) => {
                write!(fmt, "#struct{{")?;
                display_members(types.iter(), fmt)?;
                write!(fmt, "}}")
            },
            Expr::Sum(types) => {
                write!(fmt, "#sum{{")?;
                display_expr_list(types.iter(), fmt)?;
                write!(fmt, "}}")
            },
            Expr::Universe => { write!(fmt, "#universe") }
            Expr::Symbol(s) => write!(fmt, "{}", s)
        }
    }
}

fn display_expr_list<'a, I>(mut exprs: I, fmt: &mut Formatter) -> Result<(), fmt::Error> where
    I: Iterator<Item=&'a Expr> {
    if let Some(first) = exprs.next() {
        write!(fmt, "{}", first)?;
        for expr in exprs { write!(fmt, " {}", expr)? }
        Ok(())
    } else {
        Ok(())
    }
}

fn display_members<'a, I>(mut members: I, fmt: &mut Formatter) -> Result<(), fmt::Error> where
    I: Iterator<Item=(&'a IStr, &'a Expr)> {
        if let Some(first) = members.next() {
            write!(fmt, "{} : {}", first.0, first.1)?;
            for mem in members { write!(fmt, ", {} : {}", mem.0, mem.1)? }
            Ok(())
        } else {
            Ok(())
        }
}

#[cfg(test)]
mod tests {
    use super::Expr;
    #[test]
    fn nils_print_properly() {
        assert_eq!(
            format!("{}", Expr::Sexpr(vec![])),
            "()"
        );
        assert_eq!(
            format!("{}", Expr::Tuple(vec![])),
            "{}"
        );
    }
}
