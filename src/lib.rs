#![feature(never_type)]
pub mod parser;
pub mod context;
pub mod builder;
pub mod inference;
#[cfg(feature="codegen_llvm")]
pub mod codegen_llvm;

/// The integer type used to index isotope tuples
pub type TupleIx = u32;
/// The integer type used to index isotope values
pub type ValueIx = usize;
