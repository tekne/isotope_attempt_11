/*!
Generate IR for the synthesizable values in an isotope `Context`'s root
*/

use crate::context::Context;

/// The isotope code generator
pub struct Codegen<'a, M> {
    /// This code generator's borrowed context. Mutable to allow inference as necessary.
    ctx: &'a mut Context<M>
}

impl<'a, M> Codegen<'a, M> {
    /// Create a new code generator borrowing a given context
    pub fn new(ctx: &'a mut Context<M>) -> Codegen<'a, M> { Codegen { ctx } }
    /// Immutably borrow this code generator's underlying context
    pub fn ctx(&self) -> &Context<M> { &self.ctx }
}
