/*!
The isotope context builder
*/

use crate::context::{
    Context, ValId,
    define::{ValueKind, TypeAssertion}
};

pub mod tuple;
pub mod expr;
pub mod product;
pub mod projection;

/// A builder for an isotope context
#[derive(Debug)]
pub struct ContextBuilder<'a, M> {
    /// The borrowed underlying context
    ctx: &'a mut Context<M>,
    /// The unit type associated with this builder
    unit: ValId,
    /// The nil value associated with this builder
    nil: ValId,
    /// The empty type associated with this builder
    empty: ValId,
    /// The finite universe associated with this builder
    finite: ValId,
    /// The small universe associated with this builder
    small: ValId,
    /// A temporary universe associated with this builder
    temp: ValId
}

impl<'a, M> ContextBuilder<'a, M> {
    /// Create a new `ContextBuilder`, borrowing the given context. May create values.
    pub fn new(ctx: &'a mut Context<M>) -> Self {
        let small = ctx.define_universe(None).expect("Small is valid");
        let finite = ctx.define_finite(small.into()).expect("Finite is valid");
        let unit = ctx.define_val(
            ValueKind::Product,
            None.into_iter(),
            TypeAssertion::is_ty(finite).into()
        ).expect("Unit is valid");
        let nil = ctx.define_val(
            ValueKind::Tuple,
            None.into_iter(),
            TypeAssertion::is_ty(unit).into()
        ).expect("Nil is valid");
        let empty = ctx.define_inductive_args(
            None,
            None.into_iter(),
            TypeAssertion::is_ty(finite).into()
        ).expect("Empty is valid");
        let temp = ctx.define_universe(None).expect("Temp is valid");
        ContextBuilder { ctx, unit, nil, empty, finite, small, temp }
    }
    /// Get the unit type of this builder
    #[inline] pub fn unit(&self) -> ValId { self.unit }
    /// Get the nil of this builder
    #[inline] pub fn nil(&self) -> ValId { self.nil }
    /// Get the empty type of this builder
    #[inline] pub fn empty(&self) -> ValId { self.empty }
    /// Get the finite universe of this builder
    #[inline] pub fn finite(&self) -> ValId { self.finite }
    /// Get the small universe of this builder
    #[inline] pub fn small(&self) -> ValId { self.small }
    /// Immutably borrow the underlying context
    #[inline] pub fn ctx(&self) -> &Context<M> { self.ctx }
    /// Check whether two values are judgementally equal, compressing paths
    /// in the process. To do this with an immutable reference, use `ctx()`
    #[inline] pub fn je(&mut self, l: ValId, r: ValId) -> Result<ValId, (ValId, ValId)> {
        self.ctx.je_mut(l, r)
    }
    /// Get a mutable reference to this value's metadata, if any
    #[inline] pub fn get_meta_mut(&mut self, v: ValId) -> Option<&mut M> {
        self.ctx.def_mut(v).get_meta_mut()
    }
    /// Try to set the metadata for a value. Return the old value or the input on failure
    #[inline] pub fn try_set_meta(&mut self, v: ValId, m: Option<M>)
    -> Result<Option<M>, Option<M>> {
        self.ctx.def_mut(v).try_set_meta(m)
    }
}

/// State for built isotope values
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum BuiltState {
    /// A value which may be depended on by other values
    Used,
    /// A value which currently has no other values depending on it
    Free,
    /// A value which has not been given an ID before
    New
}
