use crate::{
    TupleIx,
    context::{
        ValId, Definition, CompoundKind, ValueEdge,
        define::{ValIn, ValueKind}
    }
};
use super::{
    ContextBuilder
};

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum BuildProjError {
    InvalidProj { output: TupleIx, nesting: u32 }
}

impl<'a, M> ContextBuilder<'a, M> {
    /// Build a projection. Normalizes.
    #[inline] pub fn build_projection(&mut self, source: ValId, output: TupleIx)
    -> Result<ValId, BuildProjError> {
        self.proj_helper(source, output, 0)
    }
    /// A helper for projection building, keeping track of nesting
    fn proj_helper(&mut self, source: ValId, output: TupleIx, nesting: u32)
    -> Result<ValId, BuildProjError> {
        match self.ctx().def(source) {
            Definition::Compound(c) => match c.kind {
                // Exprs are non-normalizable at the first level, so just return
                // a structural edge out. TODO: add quote expr normalization
                CompoundKind::Expr => {
                    self.ctx.define_val(
                        ValueKind::Expr,
                        Some(ValIn(source, ValueEdge::proj(0, output))).into_iter(),
                        None //TODO: typing!
                    ).map_err(|err| err)
                },
                // Tuples are trivially normalizable at the first level. Just return
                // the input node as the output. If there is no such node, it's an error
                CompoundKind::Tuple => {
                    let mut members = self.ctx.value_deps(source)
                        .filter_map(
                            |(edge, val)| if edge.input() == output {Some((edge, val))} else {None}
                        );
                    if let Some((edge, val)) = members.next() {
                        //TODO: check for duplicates?
                        std::mem::drop(members);
                        match edge {
                            ValueEdge::Expr(_) => {
                                Ok(val)
                            },
                            ValueEdge::Proj(p) => {
                                self.proj_helper(source, p.output, nesting + 1)
                            }
                        }
                    } else {
                        Err(BuildProjError::InvalidProj{ output, nesting })
                    }
                },
                _ => unimplemented!()
            }
            Definition::Primitive(_) => unimplemented!()
        }
    }
}
