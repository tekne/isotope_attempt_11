use crate::context::{
    ValId
};
use super::{
    ContextBuilder
};

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum BuildExprError {

}

impl<'a, M> ContextBuilder<'a, M> {
    /// Build an expression. Normalizes.
    pub fn build_expr<V>(&mut self, mut values: V)
    -> Result<ValId, BuildExprError> where
        V: Iterator<Item=ValId> {
        if let Some(first) = values.next() {
            if let Some(_second) = values.next() {
                // Normalization
                match first {
                    _ => unimplemented!()
                }
            } else {
                // Normalize the expression with one element to just that element
                //TODO: think about typing in this case
                Ok(first)
            }
        } else {
            // Normalize the empty expression to nil
            Ok(self.nil())
        }
    }
}
