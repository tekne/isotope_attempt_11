use crate::context::{
    define::ValueKind,
    ValId
};
use super::{
    ContextBuilder
};

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum BuildTupleError {

}

impl<'a, M> ContextBuilder<'a, M> {
    /// Build a tuple of a set of values. Normalizes.
    pub fn build_tuple<V>(&mut self, mut values: V)
    -> Result<ValId, BuildTupleError> where
        V: Iterator<Item=ValId> {
        if let Some(first) = values.next() {
            if let Some(second) = values.next() {
                self.ctx.define_val(
                    ValueKind::Tuple,
                    [first, second].iter().copied().chain(values).enumerate().map(|v| v.into()),
                    //TODO: typing
                    None)
                .map_err(|err| err)
            } else {
                // Normalize the tuple with one element to just that element
                //TODO: think about typing in this case
                Ok(first)
            }
        } else {
            // Normalize the empty tuple to nil
            Ok(self.nil())
        }
    }
}
