use crate::context::{
    define::ValueKind,
    ValId
};
use super::{
    ContextBuilder
};

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum BuildProductError {

}

impl<'a, M> ContextBuilder<'a, M> {
    /// Build a product of a set of types. Normalizes.
    pub fn build_product<V>(&mut self, mut values: V)
    -> Result<ValId, BuildProductError> where
        V: Iterator<Item=ValId> {
        if let Some(first) = values.next() {
            if let Some(second) = values.next() {
                self.ctx.define_val(
                    ValueKind::Product,
                    [first, second].iter().copied().chain(values).enumerate().map(|v| v.into()),
                    //TODO: typing
                    None)
                .map_err(|err| err)
            } else {
                // Normalize the product with one type to just that type
                //TODO: think about typing in this case
                Ok(first)
            }
        } else {
            // Normalize the empty product to the unit type
            Ok(self.unit())
        }
    }
}
