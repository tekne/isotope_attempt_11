/*!
Type inference subroutines for the isotope context builder
*/
//TODO: think of a TypeId, but don't overcomplicate things *yet* like last time...
use derive_more::From;

use crate::context::{ValId, TypeError};

/// The inferred type of a given type
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[derive(From)]
pub enum InferredType {
    /// The value has the given type
    HasType(ValId),
    /// The value is a root universe. We avoid inferring universe types to keep the graph finite
    RootUniverse{ is_finite: bool }
}

/// A type inference error
#[derive(Debug, Clone, Eq, PartialEq)]
#[derive(From)]
pub enum InferenceError {
    /// Not enough information to complete type inference
    NotEnoughInformation(NotEnoughInformation),
    /// A type error (i.e. an invalid type was inferred)
    TypeError(TypeError)
}

/// An error when there is not enough information to complete type inference
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum NotEnoughInformation {
    //TODO: this
}
