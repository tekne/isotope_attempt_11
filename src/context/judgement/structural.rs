use crate::inference::InferredType;
use crate::context::{
    Context, ValId,
    Definition, CompoundKind, IntroEdge
};

macro_rules! structurally_eq_def {
    ($mslf: expr, $s:expr, $t:expr, $try_je:ident) => {{
        use Definition::*;
        use CompoundKind::*;
        let s = $s;
        let t = $t;
        if s == t { return Ok(s) }
        let sd = $mslf.def(s);
        let td = $mslf.def(t);
        let (sd, td) = match (sd, td) {
            (Compound(sd), Compound(td)) => (sd, td),
            // TODO: check direct primitive equality
            _ => {
                unimplemented!()
            }
        };
        if sd.kind != td.kind {
            return if $mslf.is_nil(s) && $mslf.is_nil(t) { Ok(s) }
                else if $mslf.is_unit(s) && $mslf.is_unit(t) { Ok(s) }
                else { Err((s, t)) }
        }
        match sd.kind {
            e @ Expr | e @ Tuple | e @ Product => {
                let mut svs: Vec<_> = $mslf.value_deps(s).collect();
                let mut tvs: Vec<_> = $mslf.value_deps(t).collect();
                if svs.len() != tvs.len() { Err((s, t)) }
                else if svs.len() == 0 && e == Expr {
                    match $mslf.get_type(s) {
                        Some(InferredType::HasType(ty)) if $mslf.is_unit(ty) => {},
                        _ => { return Err((s, t)) }
                    }
                    match $mslf.get_type(t) {
                        Some(InferredType::HasType(ty)) if $mslf.is_unit(ty) => {},
                        _ => { return Err((s, t)) }
                    }
                    Ok(s)
                }
                else {
                    svs.sort_unstable_by_key(|(e, _)| e.input());
                    tvs.sort_unstable_by_key(|(e, _)| e.input());
                    for ((se, sv), (te, tv)) in svs.iter().zip(tvs.iter()) {
                        //TODO: handle projections properly, or maybe make projections
                        // into a node for easier uniform handling...
                        // Give up on non-mutable judgemental equality in some cases?
                        // Or do these cases just return false...
                        if se == te { $mslf.$try_je(*sv, *tv)?; } else { return Err((s, t)) }
                    }
                    Ok(s)
                }
            },
            Lambda | Pi | Inductive => {
                let mut sis: Vec<_> = $mslf.intro_deps(s).collect();
                let mut tis: Vec<_> = $mslf.intro_deps(t).collect();
                if sis.len() != tis.len() { return Err((s, t)) }
                let ie_order = |ie| {
                    use IntroEdge::*;
                    match ie {
                        Param(p) => (0 as u8, p, 0),
                        Result(r) => (1 as u8, r, 0),
                        Constr(a, c) => (2 as u8, a, c)
                    }
                };
                sis.sort_unstable_by_key(|(ie, _)| ie_order(*ie));
                tis.sort_unstable_by_key(|(ie, _)| ie_order(*ie));
                for ((se, sv), (te, tv)) in sis.iter().zip(tis.iter()) {
                    if se == te { $mslf.$try_je(*sv, *tv)?; } else { return Err((s, t)) }
                }
                Ok(s)
            },
            // Universes do not obey structural equality
            Universe | Finite => { Err((s, t)) }
        }
    }}
}

impl<M> Context<M> {
    /// Check if two values are structurally equal. If yes, return
    /// their "largest common value", or, if none, one of the inputs
    /// (note that in this case which input is returned is undefined).
    /// If no, return `Err((l, r))` where `l` and `r` are reduced forms
    /// of the left and right inputs
    pub fn structurally_eq(&self, l: ValId, r: ValId) -> Result<ValId, (ValId, ValId)> {
        structurally_eq_def!(self, l, r, je)
    }
    /// Check if two values are structurally equal, compresing paths
    /// along the way. If yes, return their "largest common value", or,
    /// if none, one of the inputs
    /// (note that in this case which input is returned is undefined).
    /// If no, return `Err((l, r))` where `l` and `r` are reduced forms
    /// of the left and right inputs
    pub fn structurally_eq_mut(&mut self, s: ValId, t: ValId) -> Result<ValId, (ValId, ValId)> {
        structurally_eq_def!(self, s, t, je_mut)
    }
}
