/*!
Judgemental equality in an isotope typing context
*/
use super::{
    Context, ValId, ValRef,
    Definition, CompoundKind, ValueEdge, TypeEdge
};

pub mod forest;
pub mod structural;

impl<M> Context<M> {
    /// Check if two values are judgementally equal. If yes, return
    /// their "largest common value", or, if none, one of the inputs
    /// (note that in this case which input is returned is undefined).
    /// If no, return `Err((l, r))` where `l` and `r` are reduced forms
    /// of the left and right inputs
    pub fn je(&self, l: ValId, r: ValId) -> Result<ValId, (ValId, ValId)> {
        if l == r { return Ok(l) }
        // Follow any chains
        let l = self.follow_chain(l);
        let r = self.follow_chain(r);
        // Then check for structural equality
        self.structurally_eq(l, r)
    }
    /// Check if two values are judgementally equal, performing path
    /// compression to make future lookups faster. If yes, Return their
    /// "largest common value", which is guaranteed to exist
    /// (since, if one does not, path compression will make one of the
    /// inputs into it).
    /// *If* the left input is a root definition, never modify it
    /// This is to allow correct usage from within a `ContextBuilder`
    /// (so as not to mess up nil, finite, etc).
    /// If no, return `Err((s, t))`, where s and t are reduced forms of
    /// the inputs.
    pub fn je_mut(&mut self, s: ValId, t: ValId) -> Result<ValId, (ValId, ValId)> {
        if s == t { return Ok(s) }
        // Follow any chains
        let s = self.compress_chain(s);
        let t = self.compress_chain(t);
        // Then check for structural equality
        let res = self.structurally_eq_mut(s, t);
        // If things are equal, link. Otherwise, return the error
        if res.is_ok() { self.direct_link(s, t); Ok(s) } else { res }
    }
    /// Check if a value is judgementally equal to nil
    pub fn is_nil(&self, v: ValId) -> bool { self.is_nil_node(self.follow_chain(v)) }
    /// Check if a value is judgementally equal to unit
    pub fn is_unit(&self, v: ValId) -> bool { self.is_unit_node(self.follow_chain(v)) }
    /// Check if a value is a nil node
    pub fn is_nil_node(&self, v: ValId) -> bool {
        match self.def(v) {
            Definition::Compound(c) => self.c_is_nil(v, c.kind),
            _ => false
        }
    }
    /// Check if a value is a unit node
    pub fn is_unit_node(&self, v: ValId) -> bool {
        match self.def(v) {
            Definition::Compound(c) => self.c_is_unit(v, c.kind),
            _ => false
        }
    }
    /// Check if a value is nil, given a compound kind and an assertion it is terminal
    fn c_is_nil(&self, v: ValId, k: CompoundKind) -> bool {
        use CompoundKind::*;
        match k {
            Expr => if self.value_deps(v).next().is_none() {
                if let Some((e, t)) = self.type_deps(v).next() {
                    match e {
                        TypeEdge::Type => self.is_unit(t),
                        TypeEdge::SameType => self.is_nil(t)
                    }
                } else {
                    false
                }
            } else { false },
            Tuple => self.value_deps(v).next().is_none(),
            //TODO: think about allowing parametrized nil...
            Lambda => self.intro_deps(v).next().is_none(),
            _ => false
        }
    }
    /// Check if a value is unit, given a compound kind and an assertion it is terminall
    fn c_is_unit(&self, v: ValId, k: CompoundKind) -> bool {
        use CompoundKind::*;
        match k {
            Product => self.value_deps(v).next().is_none(),
            //TODO: think about allowing parametrized unit...
            Pi => self.intro_deps(v).next().is_none(),
            _ => false
        }
    }
}

impl<'a, M> PartialEq for ValRef<'a, M> {
    fn eq(&self, other: &Self) -> bool {
        if self.ctx as *const Context<M> != other.ctx as *const Context<M> {
            false
        } else {
            self.ctx.je(self.id, other.id).is_ok()
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::context::{Context, define::{ValueKind, FnIntro, TypeAssertion}};
    #[test]
    fn nil_is_judgementally_nil() {
        use ValueKind::*;
        use TypeAssertion as TyA;
        let mut ctx = Context::<()>::with_capacity(8, 4);
        let unit_1 = ctx.define_val(Product, None.into_iter(), None).expect("Valid");
        let unit_2 = ctx.define_fn_intro(
            FnIntro::Pi,
            None.into_iter(),
            None.into_iter(),
            None
        ).expect("Valid");
        let nils = [
            ctx.define_val(Expr, None.into_iter(), TyA::is_ty(unit_1).into()).expect("Valid"),
            ctx.define_val(Expr, None.into_iter(), TyA::is_ty(unit_2).into()).expect("Valid"),
            ctx.define_val(Tuple, None.into_iter(), None).expect("Valid"),
            ctx.define_val(Tuple, None.into_iter(), None).expect("Valid"),
            ctx.define_val(Tuple, None.into_iter(), TyA::is_ty(unit_1).into()).expect("Valid"),
            ctx.define_val(Tuple, None.into_iter(), TyA::is_ty(unit_2).into()).expect("Valid")
        ];
        for (i, l) in nils.iter().enumerate() {
            for (j, r) in nils.iter().enumerate() {
                if i != j { assert_ne!(l, r) }
                assert!(ctx.je(*l, *r).is_ok())
            }
        }
        for l in nils.iter() {
            for r in nils.iter() {
                assert!(ctx.je_mut(*l, *r).is_ok())
            }
        }
    }
    #[test]
    fn unit_is_judgementally_unit() {
        use FnIntro::Pi;
        use ValueKind::Product;
        use TypeAssertion as TyA;
        use std::iter::empty as ei;
        let mut ctx = Context::<()>::with_capacity(4, 2);
        let universe_1 = ctx.define_universe(None).expect("Valid");
        let universe_2 = ctx.define_universe(None).expect("Valid");
        let universes = [
            universe_1,
            universe_2,
            ctx.define_universe(universe_1.into()).expect("Valid"),
            ctx.define_universe(universe_2.into()).expect("Valid")
        ];
        let finites = [
            ctx.define_finite(None).expect("Valid"),
            ctx.define_finite(universes[0].into()).expect("Valid"),
            ctx.define_finite(universes[1].into()).expect("Valid"),
            ctx.define_finite(universes[2].into()).expect("Valid"),
            ctx.define_finite(universes[3].into()).expect("Valid")
        ];
        let mut units = Vec::new();
        units.push(ctx.define_fn_intro(Pi, ei(), ei(), None).expect("Valid"));
        for u in universes.iter() {
            units.push(ctx.define_fn_intro(Pi, ei(), ei(), TyA::is_ty(*u).into()).expect("Valid"));
            units.push(ctx.define_val(Product, ei(), TyA::is_ty(*u).into()).expect("Valid"));
        }
        for f in finites.iter() {
            units.push(ctx.define_fn_intro(Pi, ei(), ei(), TyA::is_ty(*f).into()).expect("Valid"));
            units.push(ctx.define_val(Product, ei(), TyA::is_ty(*f).into()).expect("Valid"));
        }
        for (i, lu) in units.iter().enumerate() {
            for (j, ru) in units.iter().enumerate() {
                if i != j { assert_ne!(lu, ru) }
                assert!(ctx.je(*lu, *ru).is_ok())
            }
        }
        for lu in units.iter() {
            for ru in units.iter() {
                assert!(ctx.je_mut(*lu, *ru).is_ok())
            }
        }
    }
}
