use daggy::petgraph;
use petgraph::Direction;

use super::{
    Context, ValId,
    Definition, CompoundKind, ValueEdge
};
use crate::context::define::{TypeAssertion, ValueKind, ValIn};

impl<M> Context<M> {
    // Follow a link of a chain, the head of which is asserted to be an expr or quote
    fn assert_valid_follow_link(&self, v: ValId) -> Option<ValId> {
        let mut vs = self.value_deps(v);
        match vs.next() {
            Some((ValueEdge::Expr(0), first)) => {
                if vs.next().is_some() { None } else { Some(first) }
            },
            _ => None
        }
    }
    /// Follow a link of a chain
    pub fn follow_link(&self, v: ValId) -> Option<ValId> {
        use Definition::*; use CompoundKind::*;
        match self.def(v) {
            Compound(c) => match c.kind {
                Expr | Tuple => self.assert_valid_follow_link(v),
                _ => None
            },
            Primitive(_) => None
        }
    }
    /// Follow a chain
    pub fn follow_chain(&self, mut v: ValId) -> ValId {
        while let Some(vc) = self.follow_link(v) { v = vc }
        v
    }
    /// Compress a chain
    pub fn compress_chain(&mut self, v: ValId) -> ValId {
        let origin = v;
        if let Some(mut first) = self.follow_link(v) {
            // We know v is a chain. Let's find it's end
            let end = self.follow_chain(first);
            // If the end of the chain is not the same as the first link, we need to compress
            if end != first {
                self.direct_link(end, origin);
                self.direct_link(end, first);
                while let Some(rest) = self.follow_link(first) {
                    self.direct_link(end, rest);
                    first = rest;
                }
            }
            end
        } else {
            v
        }
    }
    /// Directly link two nodes
    pub fn direct_link(&mut self, s: ValId, t: ValId) {
        if s == t { return }
        let ty = self.type_deps(t).next()
            .map(|pair| pair.into())
            .unwrap_or(TypeAssertion::same_ty(s));
        // Create a new node, which we know is the most recent created.
        // This protects other nodes from having their indexes shifted.
        self.define_val(ValueKind::Expr, Some(ValIn::id(s)).into_iter(), ty.into())
            .expect("This cannot be an error");
        // Take every edge coming out of t, and connect it to s
        let mut curr = self.value_dag.graph().first_edge(t.0, Direction::Outgoing);
        while let Some(edge) = curr {
            let weight = self.value_dag[edge];
            let (parent, child) = self.value_dag.edge_endpoints(edge)
                .expect("The edge (t, child) is asserted to exist by first_edge");
            debug_assert_eq!(parent, t.0);
            self.value_dag.add_edge(s.0, child, weight)
                .expect("s is a predecessor of t, so connecting it to t's child cannot cycle");
            curr = self.value_dag.graph().next_edge(edge, Direction::Outgoing);
        }
        // Delete t
        self.value_dag.remove_node(t.0);
        // And lo and behold, the new node's index is now t
    }
    /// Link two nodes, with compression. Note we do *not* follow the chain of `t`,
    /// since that may change values not depending on it.
    pub fn link(&mut self, s: ValId, t: ValId) {
        let s = self.compress_chain(s);
        self.direct_link(s, t);
    }
}
