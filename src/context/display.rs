/*!
Utilities for displaying values in a context as S-expressions.
*/
use std::fmt::{self, Display, Formatter};
use itertools::{Itertools, Either};

use crate::inference::InferredType;
use super::{
    ValRef, Definition,
    CompoundKind, ValueEdge, IntroEdge,
    ValId
};

pub trait TryDisplay {
    type AsDisplay: ?Sized + Display;
    fn to_disp(&self) -> Option<&Self::AsDisplay>;
}

/// An isotope value coupled with an argument formatter and a reference to its context
#[derive(Debug, Copy, Clone)]
pub struct DisplayValId<'a, M, F> {
    pub val: ValRef<'a, M>,
    pub arg_formatter: F
}

impl<'a, M> ValRef<'a, M> {
    pub fn to_disp<'b, F>(self, arg_formatter: &'b F) -> DisplayValId<'a, M, &'b F> where
        F: 'b,
        DisplayValId<'a, M, &'b F>: Display {
        DisplayValId { val : self, arg_formatter }
    }
}

pub fn arity_display<M>(_: bool, _: ValRef<M>, fmt: &mut Formatter) -> Result<(), fmt::Error> {
    write!(fmt, "*")
}

pub fn simple_rec_display<M>(_: bool, val: ValRef<M>, fmt: &mut Formatter)
    -> Result<(), fmt::Error> {
    write!(fmt, "{}", DisplayValId{ val, arg_formatter : &simple_rec_display })
}

pub fn simple_reg_display<M>(_param: bool, val: ValRef<M>, fmt: &mut Formatter)
    -> Result<(), fmt::Error> {
    write!(fmt, "%{}", val.id.0.index())
}

pub fn named_reg_display<M>(param: bool, val: ValRef<M>, fmt: &mut Formatter)
    -> Result<(), fmt::Error> where M: TryDisplay {
    match val.get_meta().map(|d| d.to_disp()).unwrap_or(None) {
        Some(meta) => write!(fmt, "{}", meta),
        None => simple_reg_display(param, val, fmt)
    }
}

pub fn named_rec_display<M>(_param: bool, val: ValRef<M>, fmt: &mut Formatter)
    -> Result<(), fmt::Error> where M: TryDisplay {
    match val.get_meta().map(|d| d.to_disp()).unwrap_or(None) {
        Some(meta) => write!(fmt, "{}", meta),
        None => write!(fmt, "{}", val.to_disp(&named_rec_display))
    }
}

impl<'a, M, F> DisplayValId<'a, M, F> where F: Clone {
    #[inline]
    pub fn to_disp(&self, ix: ValId) -> Self where {
        DisplayValId {
            val: self.val.ctx.bind(ix),
            arg_formatter: self.arg_formatter.clone()
        }
    }
}

impl<'a, M, F> DisplayValId<'a, M, &F> where
    F: Fn(bool, ValRef<M>, &mut Formatter) -> Result<(), fmt::Error> {
    fn display_argument(&self, (edge, val): (ValueEdge, ValId), fmt: &mut Formatter)
    -> Result<(), fmt::Error> {
        match edge {
            ValueEdge::Expr(_) => (self.arg_formatter)(false, self.val.ctx.bind(val), fmt),
            ValueEdge::Proj(p) => {
                write!(fmt, "#proj({} ", p.output)?;
                (self.arg_formatter)(false, self.val.ctx.bind(val), fmt)?;
                write!(fmt, ")")
            }
        }
    }
    fn print_ity(&self, ity: InferredType, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        match ity {
            InferredType::HasType(v)
                => (self.arg_formatter)(false, self.val.ctx.bind(v), fmt),
            InferredType::RootUniverse { is_finite, ..} => {
                if is_finite {
                    write!(fmt, "#finite")
                } else {
                    write!(fmt, "#universe")
                }
            }
        }
    }
    fn print_opt_ity(&self, opt_ity: Option<InferredType>, fmt: &mut Formatter)
    -> Result<(), fmt::Error> {
        if let Some(ity) = opt_ity { self.print_ity(ity, fmt) } else { write!(fmt, "?") }
    }
}

impl<'a, M, F> Display for DisplayValId<'a, M, &F> where
    F: Fn(bool, ValRef<M>, &mut Formatter) -> Result<(), fmt::Error> {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        use CompoundKind::*;
        match self.val.def() {
            Definition::Compound(c) => {
                match c.kind {
                    d @ Product | d @ Expr | d @ Tuple => {
                        let mut arguments: Vec<_> = self.val.value_deps().collect();
                        arguments.sort_unstable_by_key(|(edge, _val)| edge.input());
                        let mut first = true;
                        match d {
                            Expr => write!(fmt, "("),
                            Product => write!(fmt, "#product("),
                            Tuple => write!(fmt, "{{"),
                            _ => unreachable!()
                        }?;
                        for arg in arguments {
                            if first { first = false } else { write!(fmt, " ")? }
                            self.display_argument(arg, fmt)?;
                        }
                        match d {
                            Expr | Product => write!(fmt, ")"),
                            Tuple => write!(fmt, "}}"),
                            _ => unreachable!()
                        }
                    },
                    d @ Lambda | d @ Pi => {
                        use IntroEdge::*;
                        //TODO: optimize?
                        let (mut params, mut results): (Vec<_>, Vec<_>) = self.val.intro_deps()
                            // Filter out constructors, which are an error.
                            // Should probably log a warning, or something
                            .filter(|(edge, _val)| match edge {
                                Constr(..) => false,
                                _ => true
                            })
                            .partition_map(|(edge, val)| match edge {
                                Param(p) => Either::Left((p, val)),
                                Result(r) => Either::Right((r, val)),
                                _ => unreachable!()
                            });
                        params.sort_unstable_by_key(|p| p.0);
                        results.sort_unstable_by_key(|r| r.0);
                        match d {
                            Lambda => write!(fmt, "#lambda(")?, Pi => write!(fmt, "#pi(")?,
                            _ => unreachable!()
                        }
                        // Print parameters
                        write!(fmt, "{{")?;
                        let mut first = true;
                        for (_, param) in params {
                            let param = self.val.ctx.bind(param);
                            if first { first = false } else { write!(fmt, " ")? }
                            (self.arg_formatter)(true, param, fmt)?;
                            write!(fmt, " : ")?;
                            self.print_opt_ity(self.val.get_type(), fmt)?
                        }
                        // Print results
                        write!(fmt, "}} {{")?;
                        write!(fmt, "}}")?;
                        // If lambda, print type
                        if d == Lambda {
                            write!(fmt, " -> ")?;
                            self.print_opt_ity(self.val.get_type(), fmt)?
                        }
                        write!(fmt, ")")
                    },
                    Inductive => unimplemented!(),
                    Universe => write!(fmt, "#universe"),
                    Finite => write!(fmt, "#finite")
                }
            },
            Definition::Primitive(_) => unimplemented!()
        }
    }
}
