/*!
The code to create new definitions in an isotope `Context`, and associated helper types
*/

use std::convert::TryInto;
use daggy::NodeIndex;

use crate::{ValueIx, TupleIx};
use super::{
    Context, Definition, ValId,
    TypeEdge, ValueEdge, ProjEdge, IntroEdge,
    CompoundKind
};

impl<M> Context<M> {
    /// Create a new definition of a given kind with a given type and kind
    fn child_with_type(&mut self, kind: Definition<M>, ty: TypeAssertion) -> NodeIndex<ValueIx> {
        let (_, child) = self.value_dag.add_child(ty.source.0, ty.edge.into(), kind);
        child
    }
    /// Create a child of the given value input
    fn val_in_child(&mut self, kind: ValueKind, val_in: ValIn) -> NodeIndex<ValueIx> {
        let ValIn(source, edge) = val_in;
        let (_, child) = self.value_dag.add_child(source.0, edge.into(), kind.into());
        child
    }
    /// Define a new value. Not type-checked, *or* normalized, so proceed with caution!
    /// Optional type assertion.
    //TODO: think about multiple value inputs...
    #[inline]
    pub fn define_val<D>(
        &mut self,
        kind: ValueKind,
        mut args: D,
        ty: Option<TypeAssertion>
    ) -> Result<ValId, !> where D: Iterator<Item=ValIn> {
        // Create starting noe via add_child to save DAG work.
        // Can just be replaced with add_node without impacting correctness
        let child = ty
            .map(|ty| self.child_with_type(kind.into(), ty))
            .or_else(|| args.next()
                .map(|val_in| self.val_in_child(kind, val_in)))
            .unwrap_or_else(|| self.value_dag.add_node(kind.into()));
        let args = args
            .map(|ValIn(source, kind)| {
                (source.0, child, kind.into())
            });
        self.value_dag.add_edges(args)
            .expect("Directly defining a new value should never cause a cycle");
        Ok(ValId(child))
    }
    /// Define a new lambda function or pi-type`
    #[inline]
    pub fn define_fn_intro<P, R>(
        &mut self,
        kind: FnIntro,
        mut parameters: P,
        mut results: R,
        ty: Option<TypeAssertion>
    ) -> Result<ValId, !> where
        P: Iterator<Item=(usize, ValId)>,
        R: Iterator<Item=(usize, ValId)> {
        // Create starting node via add_child to save DAG work.
        // Can just be replaced with add_node without impacting correctness
        let child = ty
            .map(|ty| self.child_with_type(kind.into(), ty))
            .or_else(|| parameters.next()
                .map(|(ix, val)| self.value_dag.add_child(
                    val.0,
                    IntroEdge::Param(ix.try_into().expect("Tuple index overflow")).into(),
                    kind.into()
                ).1))
            .or_else(|| results.next()
                .map(|(ix, val)| self.value_dag.add_child(
                    val.0,
                    IntroEdge::Result(ix.try_into().expect("Tuple index overflow")).into(),
                    kind.into()
                ).1))
            .unwrap_or_else(|| self.value_dag.add_node(kind.into()));

        // Add parameter edges
        //TODO: think about duplicate checking
        self.value_dag.add_edges(parameters.map(|(ix, val)| (
            val.0, child,
            IntroEdge::Param(ix.try_into().expect("Tuple index overflow")).into())))
            .expect("Directly defining a new fn-intro's parameters should never cycle");

        // Add result edges
        //TODO: think about duplicate checking
        self.value_dag.add_edges(results.map(|(ix, val)| (
            val.0, child,
            IntroEdge::Result(ix.try_into().expect("Tuple index overflow")).into())))
            .expect("Directly defining a new fn-intro's results should never cycle");
        Ok(ValId(child))
    }
    /// Define a new universe, potentially a sub-universe of another
    #[inline]
    pub fn define_universe(&mut self, other: Option<ValId>) -> Result<ValId, !> {
        let kind = CompoundKind::Universe;
        let res = other.map(|ty| self.child_with_type(kind.into(), TypeAssertion::is_ty(ty)))
            .unwrap_or_else(|| self.value_dag.add_node(kind.into()));
        Ok(ValId(res))
    }
    /// Define a finite type exptension, potentially a sub-extension of a universe or extension
    #[inline]
    pub fn define_finite(&mut self, other: Option<ValId>) -> Result<ValId, !> {
        let kind = CompoundKind::Finite;
        let res = other.map(|ty| self.child_with_type(kind.into(), TypeAssertion::is_ty(ty)))
            .unwrap_or_else(|| self.value_dag.add_node(kind.into()));
        Ok(ValId(res))
    }
    /// Helper function to get the starting Option for an inductive definition
    fn start_inductive(&mut self, name: Option<ValId>, ty: Option<TypeAssertion>)
        -> Option<NodeIndex<ValueIx>> {
        name.map(|name| {
            let (_, child) = self.value_dag.add_child(
                name.0,
                IntroEdge::Result(0).into(),
                CompoundKind::Inductive.into());
            child
        })
            .or_else(|| ty.map(
                |ty| self.child_with_type(CompoundKind::Inductive.into(), ty)
            ))
    }
    /// Define an inductive type with a given list of constructors and symbol
    /// Does *not* check strict positivity, so again, use with caution
    #[inline]
    pub fn define_inductive<C>(
        &mut self,
        name: Option<ValId>,
        constructors: C,
        ty: Option<TypeAssertion>
    ) -> Result<ValId, !> where C: Iterator, C::Item: Iterator<Item=(usize, ValId)> {
        let flattened = constructors
            .enumerate()
            .map(
                |(constructor, arguments)| arguments.map(
                    move |(argument, value)| (constructor, (argument, value))))
            .flatten();
        self.define_inductive_args(name, flattened, ty)
    }
    /// Define an inductive type with a given list of constructor arguments and symbols
    /// Does *not* check strict positivity, so again, use with caution
    #[inline]
    pub fn define_inductive_args<A>(
        &mut self,
        name: Option<ValId>,
        mut arguments: A,
        ty: Option<TypeAssertion>
    ) -> Result<ValId, !> where A: Iterator<Item=(usize, (usize, ValId))> {
        // Create starting node via add_child to save DAG work.
        // Can just be replaced with add_node without impacting correctness
        let res = self.start_inductive(name, ty)
            .or_else(|| arguments.next().map(|(constructor, (argument, value))| {
                let (_, child) = self.value_dag.add_child(
                    value.0,
                    IntroEdge::Constr(
                        constructor.try_into().expect("Tuple index overflow"),
                        argument.try_into().expect("Tuple index overflow")
                    ).into(),
                    CompoundKind::Inductive.into()
                );
                child
            }))
            .unwrap_or_else(|| self.value_dag.add_node(CompoundKind::Inductive.into()));
        let arguments = arguments.map(
            |(constructor, (argument, value))| (
                value.0, res,
                IntroEdge::Constr(
                    constructor.try_into().expect("Tuple index overflow"),
                    argument.try_into().expect("Tuple index overflow")
                ).into()
            ));
        self.value_dag.add_edges(arguments)
            .expect("Directly defining a new inductive type should never cycle");
        Ok(ValId(res))
    }
}

/// Possible kinds of a function-like introduction
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum FnIntro {
    /// A dependently typed lambda function
    Lambda,
    /// A pi-type
    Pi
}

impl From<FnIntro> for CompoundKind {
    #[inline] fn from(i: FnIntro) -> Self {
        use FnIntro::*;
        match i {
            Lambda => Self::Lambda,
            Pi => Self::Pi
        }
    }
}

impl<M> From<FnIntro> for Definition<M> {
    #[inline] fn from(i: FnIntro) -> Self {
        CompoundKind::from(i).into()
    }
}

/// Possible kinds of a value definition
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ValueKind {
    /// An S-expression
    Expr,
    /// A tuple
    Tuple,
    /// A product type
    Product
}

impl From<ValueKind> for CompoundKind {
    #[inline] fn from(v: ValueKind) -> Self {
        use ValueKind::*;
        match v {
            Expr => Self::Expr,
            Tuple => Self::Tuple,
            Product => Self::Product
        }
    }
}

impl<M> From<ValueKind> for Definition<M> {
    #[inline] fn from(v: ValueKind) -> Self {
        CompoundKind::from(v).into()
    }
}

/// An assertion about a value's type, consisting of a type edge and a source
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct TypeAssertion {
    /// The source of the type edge
    pub source: ValId,
    /// The type edge being asserted
    pub edge: TypeEdge
}

impl From<(TypeEdge, ValId)> for TypeAssertion {
    fn from((edge, source): (TypeEdge, ValId)) -> Self { Self { edge, source } }
}

impl TypeAssertion {
    /// Create the assertion "is type source"
    #[inline]
    pub fn is_ty(source: ValId) -> Self { TypeAssertion { source, edge: TypeEdge::Type } }
    /// Create the assertion "is the same type as source"
    #[inline]
    pub fn same_ty(source: ValId) -> Self { TypeAssertion { source, edge: TypeEdge::SameType } }
}

/// An optional projection of an output slot out of a value
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct ValProj {
    /// The value to project from
    pub value: ValId,
    /// The output slot to project from
    pub output: Option<TupleIx>
}

impl ValProj {
    /// Get the value edge corresponding to putting this projection into a given input
    #[inline]
    pub fn get_edge(&self, input: TupleIx) -> ValueEdge {
        match self.output {
            Some(output) => ValueEdge::Proj(ProjEdge{ input, output }),
            None => ValueEdge::Expr(input)
        }
    }
}

impl From<ValId> for ValProj { #[inline] fn from(value: ValId) -> ValProj {
    ValProj { value, output: None } } }

/// An input to a value definition
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct ValIn(pub ValId, pub ValueEdge);

impl ValIn {
    /// Get the input for an identity sexpr
    #[inline] pub fn id(v: ValId) -> ValIn { ValIn(v, ValueEdge::Expr(0)) }
}

impl<T: Into<ValProj>> From<(usize, T)> for ValIn {
    #[inline] fn from((ix, pr): (usize, T)) -> ValIn {
        let pr = pr.into();
        ValIn(pr.value, pr.get_edge(ix.try_into().expect("Tuple index overflow!")))
    }
}
