/*!
The isotope typing context system
*/
use std::convert::{TryInto, TryFrom};
use daggy::{Dag, NodeIndex, Edges, petgraph};
use petgraph::Direction;
use petgraph::visit::IntoEdgesDirected;
use petgraph::visit::EdgeRef;
use derive_more::From;

use crate::{TupleIx, ValueIx};
use crate::inference::InferredType;

pub mod define;
pub mod display;
pub mod judgement;
pub mod primitive;
use primitive::PrimitiveDef;

/// The ID of a valid isotope value.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ValId(pub NodeIndex<ValueIx>);

/// A valid isotope value in a context
#[derive(Debug)]
pub struct ValRef<'a, M> {
    id: ValId,
    ctx: &'a Context<M>
}

impl<'a, M> Clone for ValRef<'a, M> {
    #[inline] fn clone(&self) -> Self { ValRef { id: self.id, ctx: self.ctx } }
}
impl<'a, M> Copy for ValRef<'a, M> {}

impl<'a, M> ValRef<'a, M> {
    #[inline] pub fn def(&self) -> &'a Definition<M> { self.ctx.def(self.id) }
    #[inline] pub fn deps(&self) -> Edges<'a, Edge, ValueIx> { self.ctx.deps(self.id) }
    #[inline] pub fn type_deps(&self) -> impl Iterator<Item=(TypeEdge, ValId)> + 'a {
        self.ctx.type_deps(self.id)
    }
    #[inline] pub fn value_deps(&self) -> impl Iterator<Item=(ValueEdge, ValId)> + 'a {
        self.ctx.value_deps(self.id)
    }
    #[inline] pub fn intro_deps(&self) -> impl Iterator<Item=(IntroEdge, ValId)> + 'a {
        self.ctx.intro_deps(self.id)
    }
    #[inline] pub fn get_meta(&self) -> Option<&'a M> {
        self.def().get_meta()
    }
    #[inline] pub fn get_type(&self) -> Option<InferredType> { self.ctx.get_type(self.id) }
}

impl<'a, M> From<ValRef<'a, M>> for ValId { fn from(v: ValRef<'a, M>) -> ValId { v.id } }

/// An isotope typing context and associated metadata
#[derive(Debug, Clone)]
pub struct Context<M> {
    /// The underlying value definition DAG
    value_dag: Dag<Definition<M>, Edge, ValueIx>,
}

impl<M> Context<M> {
    /// Create a new, empty context with the given capacities
    pub fn with_capacity(definitions: usize, dependencies: usize) -> Self {
        let mut res = Context {
            value_dag: Dag::with_capacity(definitions + 1, dependencies)
        };
        // Add root node to value dag
        res.value_dag.add_node(CompoundKind::Tuple.into());
        res
    }
    /// Bind a value ID to a reference to this context, to get a value reference
    #[inline] pub fn bind(&self, id: ValId) -> ValRef<M> { ValRef { id, ctx: self } }
    /// Get the definition of a value in this context
    #[inline] pub fn def(&self, val: ValId) -> &Definition<M> { &self.value_dag[val.0] }
    /// Get a mutable reference to the definition of a value in this context
    #[inline] pub fn def_mut(&mut self, val: ValId) -> &mut Definition<M> {
        &mut self.value_dag[val.0]
    }
    /// Get the edges this definition depends on
    #[inline] pub fn deps(&self, val: ValId) -> Edges<Edge, ValueIx> {
        self.value_dag.edges_directed(val.0, Direction::Incoming)
    }
    /// Get the type dependencies of this definition
    #[inline] pub fn type_deps(&self, val: ValId) -> impl Iterator<Item=(TypeEdge, ValId)> + '_ {
        self.deps(val).filter_map(
            |dep| match (*dep.weight()).try_into() {
                Ok(te) => Some(
                    (te, ValId(dep.source()))
                ),
                Err(_) => None
            }
        )
    }
    /// Get a ValId for the type of this value, if any. Return None if not enough info
    #[inline] pub fn get_type(&self, val: ValId) -> Option<InferredType> {
        if let Some((edge, ty)) = self.type_deps(val).next() {
            match edge {
                TypeEdge::SameType => unimplemented!(),
                TypeEdge::Type => Some(InferredType::HasType(ty))
            }
        } else if let Definition::Compound(c) = self.def(val) {
            match c.kind {
                CompoundKind::Universe => Some(InferredType::RootUniverse{ is_finite : false }),
                CompoundKind::Finite => Some(InferredType::RootUniverse{ is_finite : true }),
                _ => None
            }
        } else {
            None
        }
    }
    /// Get the value dependencies of this definition
    #[inline] pub fn value_deps(&self, val: ValId) -> impl Iterator<Item=(ValueEdge, ValId)> + '_ {
        self.deps(val).filter_map(
            |dep| match (*dep.weight()).try_into() {
                Ok(ve) => Some(
                    (ve, ValId(dep.source()))
                ),
                Err(_) => None
            }
        )
    }
    /// Get the introduction dependencies of this definition
    #[inline] pub fn intro_deps(&self, val: ValId) -> impl Iterator<Item=(IntroEdge, ValId)> + '_ {
        self.deps(val).filter_map(
            |dep| match (*dep.weight()).try_into() {
                Ok(te) => Some(
                    (te, ValId(dep.source()))
                ),
                Err(_) => None
            }
        )
    }
    /// Get the root index of this context
    #[inline(always)] pub fn get_root(&self) -> ValId { ValId(NodeIndex::new(0)) }
}

/// An isotope value definition, which may hold associated metadata
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Definition<M> {
    /// A compound definition (i.e. a function call, tuple, etc.)
    Compound(CompoundDef<M>),
    /// A primitive definition (i.e. a natural number, etc.)
    Primitive(PrimitiveDef)
    //TODO: where do universes go? Nil? Unit? Symbols?
}

impl<M> Definition<M> {
    /// Get the metadata of this definition, if any
    pub fn get_meta(&self) -> Option<&M> {
        use Definition::*;
        match self {
            Compound(c) => c.metadata.as_ref(),
            Primitive(_) => None
        }
    }
    /// Get mutable metadata for this definition, if any
    pub fn get_meta_mut(&mut self) -> Option<&mut M> {
        use Definition::*;
        match self {
            Compound(c) => c.metadata.as_mut(),
            Primitive(_) => None
        }
    }
    /// Try to set the metadata for a value. Return the old value or the input on failure
    #[inline] pub fn try_set_meta(&mut self, mut m: Option<M>)
    -> Result<Option<M>, Option<M>> {
        use Definition::*;
        match self {
            Compound(c) => Ok({ std::mem::swap(&mut c.metadata, &mut m); m }),
            Primitive(_) => Err(m)
        }
    }
}

/// A compound isotope value definition, holding (optional) metadata and a type
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct CompoundDef<M> {
    /// The metadata ID associated with this definition
    pub metadata: Option<M>,
    /// The kind of this definition
    pub kind: CompoundKind
}

impl<M> From<CompoundKind> for CompoundDef<M> {
    #[inline] fn from(kind: CompoundKind) -> Self { CompoundDef { metadata: None, kind } }
}
impl<M> From<CompoundKind> for Definition<M> {
    #[inline] fn from(kind: CompoundKind) -> Self { Definition::Compound(kind.into()) }
}

/// Possible kinds of compound definition
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum CompoundKind {
    /// An S-expression: evaluate the first argument, applied to the rest via currying.
    /// An S-expression with a type but no arguments is a symbol (of that type).
    /// Any symbol of `nil` can be reduced to the nil tuple.
    Expr,
    /// A (potentially dependently typed) tuple of its inputs.
    /// A tuple with no arguments is `nil`, an element of the unit type.
    Tuple,
    /// A dependently typed lambda function.
    Lambda,
    /// A simple product. Its components are `Value` edges
    ///
    /// While this *can* be defined inductively, it's more efficient this way, *and*
    /// allows tuples in the `Finite` family. A product of no types is the unit type
    Product,
    /// A pi-type. Its components are `Result` edges, parametrized over sparse Parameter edges.
    ///
    /// A pi type with no `Result` or `Parameter` edges is the unit type.
    /// Only `Result` types may depend on the `Result` of an `Inductive` type
    Pi,
    /// An inductive family. Its self-symbol is a `Result` edge, its constructor arguments
    /// are `Constructor` edges
    ///
    /// The first exit edge is the defined type (family), the second is the
    /// recursor (family), and all other edges are the constructors in order
    Inductive,
    /// A polymorphic universe. Typing => containment. Scoping => join.
    Universe,
    /// A universe which does *not* contain inductive types of its elements
    Finite
}

/// An isotope edge
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[derive(From)]
pub enum Edge {
    /// An edge implying the target is in a subscope of the source, and hence
    /// cannot be defined if the source is undefined, and cannot be a dependency
    /// of the source
    ScopeEdge,
    /// An edge implying the type of the target node is dependent on the source
    TypeEdge(TypeEdge),
    /// An edge implying the value of the target node is dependent on the source
    ValueEdge(ValueEdge),
    /// An edge implying the target node is a function or inductive type defined by the source
    IntroEdge(IntroEdge)
    //TODO: Pi types?
}

/// An isotope type dependency edge
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TypeEdge {
    /// Indicates target has type source
    Type,
    /// Indicates target has the same type as source
    SameType
}

impl TryFrom<Edge> for TypeEdge {
    type Error = Edge;
    fn try_from(e: Edge) -> Result<Self, Edge> {
        match e {
            Edge::TypeEdge(e) => Ok(e),
            o => Err(o)
        }
    }
}

/// An isotope value dependency edge
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[derive(From)]
pub enum ValueEdge {
    /// Assign a whole expression as an input component of an expression
    Expr(TupleIx),
    /// Assign an output component of a definition to the input component of an expression
    Proj(ProjEdge)
}

impl ValueEdge {
    /// Get a projection value edge
    pub fn proj(input: TupleIx, output: TupleIx) -> ValueEdge {
        ValueEdge::Proj(ProjEdge { input, output })
    }
    /// Get the input slot of this value edge
    #[inline]
    pub fn input(&self) -> TupleIx {
        match self {
            Self::Expr(i) => *i,
            Self::Proj(p) => p.input
        }
    }
}

/// An isotope projection edge. Project an output slot of a value to a given input slot
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct ProjEdge {
    /// The input slot to project into
    pub input: TupleIx,
    /// The output slot to project from
    pub output: TupleIx
}

impl TryFrom<Edge> for ValueEdge {
    type Error = Edge;
    fn try_from(e: Edge) -> Result<Self, Edge> {
        match e {
            Edge::ValueEdge(e) => Ok(e),
            o => Err(o)
        }
    }
}

/// An isotope introduction dependency edge
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum IntroEdge {
    /// The nth result of a term introduction
    Result(TupleIx),
    /// The nth parameter of a term introduction
    Param(TupleIx),
    /// The nth argument of the nth constructor
    Constr(TupleIx, TupleIx)
}


impl TryFrom<Edge> for IntroEdge {
    type Error = Edge;
    fn try_from(e: Edge) -> Result<Self, Edge> {
        match e {
            Edge::IntroEdge(e) => Ok(e),
            o => Err(o)
        }
    }
}

/// An isotope type error
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TypeError {
    /// A cyclical type dependency
    Cyclical
}

impl From<DefinitionError> for TypeError {
    fn from(d: DefinitionError) -> Self {
        match d {
            DefinitionError::Cyclical => Self::Cyclical
        }
    }
}

/// An isotope definition error
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum DefinitionError {
    /// Adding this definition would cause a cycle
    Cyclical
}
