/*!
Primitive isotope values for special handling
*/
use internship::IBytes;

/// A primitive isotope value definition.
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum PrimitiveDef {
    /// The natural numbers
    Nat,
    /// The integers
    Int,
    /// A type with a finite number of values, numbered 0 to n
    FiniteType(u128),
    /// An element of a small finite type *or* a small natural number, depending on type edge used
    Small(u128),
    /// A small integer
    SmallInt(i128),
    /// The recursor for a small finite type. Also efficiently stored as an array if necessary.
    Array(u64),
    /// A type with up n bits. Isomorphic to Finite(2^n), so Small's can be typed with this
    Bits(u32),
    /// A set of n bytes. Of type `Bits(8n)`
    Bytes(IBytes)
}
